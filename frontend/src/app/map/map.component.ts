//by Arthur Fontaine
import { Component , AfterViewInit } from '@angular/core';
import * as L from 'leaflet';
import { MarkerService } from '../marker.service';


// ICON DEFINITION
const iconRetinaUrl = 'assets/marker-icon-2x.png';
const iconUrl = 'assets/marker-icon.png';
const shadowUrl = 'assets/marker-shadow.png';
const iconDefault = L.icon({
  iconRetinaUrl,
  iconUrl,
  shadowUrl,
  iconSize: [25, 41],
  iconAnchor: [12, 41],
  popupAnchor: [1, -34],
  tooltipAnchor: [16, -28],
  shadowSize: [41, 41]
});
L.Marker.prototype.options.icon = iconDefault;


@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})

export class MapComponent implements AfterViewInit{
  map: L.Map | L.LayerGroup<any>;

  constructor(private markerService: MarkerService) {}

  ngAfterViewInit(): void {
    this.map = this.createMap();
    this.addMainLayer();
    this.markerService.makeMarkers(this.map);
    this.map.panTo(new L.LatLng(0,0));
  }

  createMap(){
    const view = {
      lat : 5,
      lng : -160
    };
    const zoomlvl=2;
    return L.map('map', {
      center: [view.lat, view.lng],
      zoom: zoomlvl,
    });
  }

  addMainLayer(){
    const mainLayer = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',{
      minZoom : 2,
      maxZoom : 17,
      noWrap : false, //show multiple maps or just the main one
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    });
    mainLayer.addTo(this.map);
  }
}