import { DataSource } from '@angular/cdk/collections';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { map } from 'rxjs/operators';
import { Observable, of as observableOf, merge } from 'rxjs';
import { RequestService } from '../request.service';

// TODO: Replace this with your own data model type
export interface EventTableItem {
  place: string;
  magnitude: number;
  date : string;
  lat: number;
  lon: number;
}
/**
 * Data source for the EventTable view. This class should
 * encapsulate all logic for fetching and manipulating the displayed data
 * (including sorting, pagination, and filtering).
 */
export class EventTableDataSource extends DataSource<EventTableItem> {
  data: EventTableItem[] = [];
  paginator: MatPaginator | undefined;
  sort: MatSort | undefined;


  constructor(private requestservice : RequestService) {
    super();
    this.createDataSource();
  }

  createDataSource(){
    this.requestservice.getEvents().subscribe((res: any) => {
      for (const c of res.features) {
        const place = c.properties.place;
        const magnitude = Number(c.properties.magnitude.mag);
        const date = c.properties.origin.time;
        const lat = c.geometry.coordinates[0];
        const lon = c.geometry.coordinates[1];
        this.data.push({
          'place' : place,
          'magnitude' : magnitude,
          'date' : date, 
          'lat' : lat,
          'lon' : lon
        });
      }
    });
  }

  /**
   * Connect this data source to the table. The table will only update when
   * the returned stream emits new items.
   * @returns A stream of the items to be rendered.
   */
  connect(): Observable<EventTableItem[]> {
    if (this.paginator && this.sort) {
      // Combine everything that affects the rendered data into one update
      // stream for the data-table to consume.
      return merge(observableOf(this.data), this.paginator.page, this.sort.sortChange)
        .pipe(map(() => {
          return this.getPagedData(this.getSortedData([...this.data ]));
          //return this.data;
        }));
    } else {
      throw Error('Please set the paginator and sort on the data source before connecting.');
    }
  }

  /**
   *  Called when the table is being destroyed. Use this function, to clean up
   * any open connections or free any held resources that were set up during connect.
   */
  disconnect(): void {}

  /**
   * Paginate the data (client-side). If you're using server-side pagination,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getPagedData(data: EventTableItem[]): EventTableItem[] {
    if (this.paginator) {
      const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
      return data.splice(startIndex, this.paginator.pageSize);
    } else {
      return data;
    }
  }

  /**
   * Sort the data (client-side). If you're using server-side sorting,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getSortedData(data: EventTableItem[]): EventTableItem[] {
    if (!this.sort || !this.sort.active || this.sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      const isAsc = this.sort?.direction === 'asc';
      switch (this.sort?.active) {
        case 'place': return compare(a.place, b.place, isAsc);
        case 'magnitude': return compare(+a.magnitude, +b.magnitude, isAsc);
        case 'date': return compare(+a.date,+b.date,isAsc);
        default: return 0;
      }
    });
  }
}

/** Simple sort comparator for example ID/Name columns (for client-side sorting). */
function compare(a: string | number, b: string | number, isAsc: boolean): number {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
