import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RequestService {
  geojsonEvent : Observable<any> ; 

  constructor(private http: HttpClient) {
    this.geojsonEvent = this.http.get('http://itec.unice.fr/api/fdsnws/event/events.geoJSON');
  }

  getEvents() : Observable<any> {
    return this.geojsonEvent;
  }

  makeRequest(link : string): Observable<any>{
    return this.http.get(link);
  }
}
