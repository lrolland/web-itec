//by Arthur Fontaine
import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { RequestService } from '../request.service';


let values : any; //store the result of the selected event (row)
let stationChoice : string= ""; //build link when you click on a station
let allStation : string=""; // build a link with all the station from tableResult in case you don't specify which station you need

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent{
  tableResult : Observable<any>; //store the restult of station research
  send : boolean = false;
  maxRadius : FormControl = new FormControl('');
  startDate : FormControl = new FormControl('');
  endDate : FormControl = new FormControl('');
  stationCode : FormControl = new FormControl('');
  networkCode : FormControl = new FormControl('');

  constructor( private requestservice: RequestService) {}
  /**
   * show panel which allow to find the station around the selected event, called from event-table.component.js
   * @param row : {}
   */
  stationQuery(row : {}){
    this.resetForms();
    values = row;
    var form = document.getElementById('station');
    this.endQuery('data', false);
    if(form != null && form.style.display != "block"){ form.style.display = "block"; }
  }
  /**
   * Show panel which allow find the data from the event and the station selected 
   */
  dataQuery(){
    var form = document.getElementById('data');
    this.endQuery('station', false);
    if(form != null && form.style.display != "block"){ form.style.display = "block"; }
  }
  /**
   * Close the search panel from the id, also trigger the request if the search value is true
   * @param id string
   * @param search boolean 
   */
  endQuery(id:string, search:boolean){
    var form = document.getElementById(id);
    if(form != null && form.style.display != "none"){ form.style.display = "none"; }
    if(search){ this.chooseSearch(id); }
  }

  resetForms(){
    this.maxRadius.reset();
    this.networkCode.reset();
    this.send = false;
    this.startDate.reset();
    this.endDate.reset();
    this.stationCode.reset();
  }
  /**
   * Trigger the right request depending on the given id
   * @param id string
   */
  chooseSearch(id:string){
    if(id == "station"){
      this.searchStation();
      this.dataQuery();
    }else{
      this.searchData();
    }
  }
  /**
   * Create the request url for station and store the result in this.tableResult, then trigger showResult() 
   */
  searchStation() {
    var link : string = 'http://itec.unice.fr/api/fdsnws/station/query?'+"maxradius="+this.maxRadius.value+"&lat="+values.lat+"&lon="+values.lon;
    if(this.endDate.value != ''){
      link+="&end="+this.endDate.value+"T00:00:00.000";
    }else{
      link+="&end="+values.date ;
    }
    if(this.startDate.value != ''){
      link+="&start="+this.startDate.value+"T00:00:00.000";
    }
    if(this.stationCode.value != ''){
      link+="&sta="+this.stationCode.value;
    }
    if(this.networkCode.value != ''){
      link+="&net="+this.networkCode.value;
    }
    this.tableResult = this.requestservice.makeRequest(link);
    this.showResult();
  }
  /**
   * Create the request url for data and download them
   */
  searchData() {
    var result = document.getElementById('result');
    if(result != null) result.style.display="none";
    var link : string = 'http://itec.unice.fr/api/fdsnws/dataselect/query?';
    console.log(values)
    if(this.endDate.value != ''){
      link+="end="+this.endDate.value+"T00:00:00.000";
    }else{
      link+="end="+values.date;
    }
    if(this.startDate.value != ''){
      link+="&start="+this.startDate.value+"T00:00:00.000";
    }
    if(this.stationCode.value != ''){
      link+="&sta="+this.stationCode.value.toUpperCase();
    }else if(stationChoice != ""){
      link+="&sta="+stationChoice.toUpperCase();
    }else{
      link+="&sta="+allStation.toUpperCase();
    }
    if(this.networkCode.value != ''){
      link+="&net="+this.networkCode.value;
    }
    window.location.href=link;
  }

  /**
   * Show the result of the stationQuery()
   */
  showResult() {
    this.tableResult.subscribe((res :any)=>{
      var result = document.getElementById('result');
      if(result != null){
        while (result.firstChild) {
          result.removeChild(result.firstChild);
        }
        var head = ['Station', 'Creation', 'Network'];
        let table = document.createElement('table');
        table.id="stationTable";
        let row = table.insertRow();
        for(const n of head){
          let th = document.createElement('th');
          th.innerText=n;
          row.appendChild(th);
        }
        for (const elem of res.stations){
          let row = table.insertRow();
          let cell = row.insertCell();
          cell.innerText=elem.station_code;
          let cell2 = row.insertCell();
          cell2.innerText=elem.creationDate;
          let cell3 = row.insertCell();
          cell3.innerText=elem.network_code;
          row.onclick=function(){
            if(stationChoice == ""){
              stationChoice+=cell.innerText;
            }else{
              stationChoice+=","+cell.innerText;
            }
          }
          row.style.cursor="pointer";
          if(allStation == ""){
            allStation+=cell.innerText;
          }else{
            allStation+=","+cell.innerText;
          }
        }
        result.appendChild(table);
        let btn = document.createElement('button');
        btn.onclick = function(){
          var result = document.getElementById('result');
          if(result != null && result.style.display != "none"){ result.style.display = "none"; }
        }
        btn.innerText="Close";
        result.appendChild(btn);
        result.style.display="block";
      }
    });
  }
}
