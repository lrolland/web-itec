# Request data from the api

In the browser or with curl 

## To request events 

    curl http://itec.unice.fr/api/fdsnws/event/events.geoJSON

## To request stations
If you want a list of stations : 

    curl http://itec.unice.fr/api/fdsnws/station/stations.json

If you need to add argument to your request :

    curl http://itec.unice.fr/api/fdsnws/station/query?

And you can add the argument you want in this list :

* network code : net=
* station code : sta=
* start date : start=
* end date : end=
* channel : cha=
* location : loc= 
* latitude : lat=
* longitude : lon=
* maxradius : maxradius=
  
An exemple would be : 

    curl http://itec.unice.fr/api/fdsnws/station/query?net=GI&start=10-02-2010T00:00:00.000

## To request data files

    curl http://itec.unice.fr/api/fdsnws/dataselect/query? 

You have to add, as for the stations, the suffixe : 

* network : net=
* station : sta=
* start date : start=
* end date : end=
* channel : cha=
* location : loc=

exemple : 

    curl http://itec.unice.fr/api/dataselect/query?net=RI&start=10-02-2010T00:00:00.000&sta=CONZ

