import os, zipfile, io, pathlib, datetime
from flask import Blueprint, request, send_from_directory, jsonify, send_file
from obspy import read

dataselect = Blueprint('dataselect', __name__)

MSEED_FOLDER_SERVER = "/var/www/web-itec/backend/SDS/"
MSEED_FOLDER = './backend/SDS/' # must be in the web-itec directory

@dataselect.route('/', methods=['GET'])
def home():
    return '<h1>Dataselect</h1>'

@dataselect.route('/version', methods=['GET'])
def version():
    return 'version : 0'

# @dataselect.route('/get-file/<file_name>', methods=['GET'])
# def get_file(file_name):
#    filename = f"{file_name}"
#    try:
#        return send_from_directory(MSEED_FOLDER_SERVER, filename=filename, as_attachment=True)
#    except FileNotFoundError:
#        return "Error 204: file not found"

@dataselect.route('/query', methods=['GET'])
def query():
    args = {}
    if 'net' in request.args :
        args['network'] = request.args['net']
    if 'start' in request.args :
        args['starttime'] = datetime.datetime.strptime(request.args['start'], '%Y-%m-%dT%H:%M:%S.%f')
    if 'end' in request.args : 
        args['endtime'] = datetime.datetime.strptime(request.args['end'], '%Y-%m-%dT%H:%M:%S.%f')
    if 'sta' in request.args : 
        args['station'] = request.args['sta']
    if 'cha' in request.args : 
        args['channel'] = request.args['cha']
    if 'loc' in request.args :
        args['location'] = request.args['loc']

    fileList = findFiles(args, MSEED_FOLDER_SERVER)
    if fileList != []:
        data = io.BytesIO()
        with zipfile.ZipFile(data, mode='w') as z: #create a zip but it may be better to use obspy to create an mseed file 
            for file in fileList:
                z.write(file, file.name)
        data.seek(0)
        return send_file(
            data,
            mimetype='application/zip',
            as_attachment=True,
            attachment_filename='data.zip'
        )
    else:
        return "Error 204 <br/>No data for this query"




###### Functions #######
# find the files in the SDS architecture, use the string[] returned for os.listdir to sort the folders
def findFiles(args, path):
    res = []
    for date in yearCheck(os.listdir(path), args):
        print(date)
        for net in valueCheck(os.listdir(path+date),'network', args):
            for sta in valueCheck(os.listdir(path+date+'/'+net), 'station', args):
                for cha in valueCheck(os.listdir(path+date+'/'+net+'/'+sta), 'channel', args):
                    for file in pathlib.Path(path+date+'/'+net+'/'+sta+'/'+cha+'/').iterdir():
                        filename = file.name.split('.')
                        if filename[-1] != 'txt' and addFile(filename, args): 
                            res.append(file)
    return res

# check the date and the location of the file, return true if is should be returned
def addFile(filename, args):
    fileDate = datetime.datetime.strptime(filename[5]+"-"+filename[6]+"T00:00:00", "%Y-%jT%H:%M:%S")
    print(fileDate)
    if 'starttime' in args and 'location' in args and 'endtime' in args:
        if args['starttime'] > fileDate or fileDate > args['endtime'] or filename[2] != args['location']:
            return False
    elif 'starttime' in args and 'endtime' in args:
        if fileDate < args['starttime'] or fileDate > args['endtime']:
            return False
    elif 'location' in args and 'starttime' in args:
        if args['starttime'] < fileDate or filename[2] != args['location']:
            return False
    elif 'location' in args and 'endtime' in args:
        if fileDate > args['endtime'] or filename[2] != args['location']:
            return False
    elif 'location' in args:
        if filename[2] != args['location']:
            return False
    elif 'starttime' in args:
        if args['starttime'] > fileDate:
            return False
    elif 'endtime' in args:
        if args['endtime'] < fileDate:
            return False
    return True

# check the year of the folder and return a list of the allowed years as string
def yearCheck(folder, args):
    if 'starttime' not in args and 'endtime' not in args:
        return folder
    else:
        res = [] 
        for date in folder:
            if 'starttime' in args and 'endtime' in args:
                if datetime.datetime.strftime(args['starttime'], '%Y') <= date and  date <= datetime.datetime.strftime(args['endtime'], "%Y"):
                    res.append(date)
            elif 'starttime' in args:
                if datetime.datetime.strftime(args['starttime'], '%Y') <= date:
                    res.append(date)
            else:
                if date <= datetime.datetime.strftime(args['endtime'], '%Y'):
                    res.append(date)
    return res

# check the value and return the allowed ones as a table of string
def valueCheck(folder, argument, args):
    if argument not in args:
        return folder
    else:
        res = []
        for arg in folder:
            if arg in args[argument]:
                res.append(arg)
    return res
    
