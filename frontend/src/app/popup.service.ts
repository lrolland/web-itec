import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PopupService {

  constructor() { }

  makeEventPopup(data : any){
    return '' +
      '<b>Place : '+data.place+'</b>'+
      '<div>Magnitude : '+data.magnitude.mag+'</div>'+
      '<div>Date : '+data.origin.time+'</div>'
  }
}
