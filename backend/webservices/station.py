from webservices.event import XML_FOLDER, XML_FOLDER_SERVER
from flask import Blueprint, request
import xml.etree.ElementTree as ET

station = Blueprint('station', __name__)

@station.route('/', methods=['GET'])
def home():
    return '<h1>Station</h1>'

@station.route('/version', methods=['GET'])
def version():
    return 'version : 0'

@station.route('/stations.json', methods=['GET'])
def getStationGEOJSON():
    file = ET.parse(XML_FOLDER_SERVER+'/stations.xml')
    root = file.getroot()
    data = {}
    for network in root.findall(".//{http://www.fdsn.org/xml/station/1}Network"):
        data[network.get('code')] = ({
            'startDate' : network.get('startDate'),
            'stations' : []
        })
        for station in network.findall(".//{http://www.fdsn.org/xml/station/1}Station"):
            data[network.get('code')]['stations'].append({
                'code' : station.get('code'),
                'latitude' : station.find('.//{http://www.fdsn.org/xml/station/1}Latitude').text,
                'longitude' : station.find('.//{http://www.fdsn.org/xml/station/1}Longitude').text,
                'elevation' : station.find('.//{http://www.fdsn.org/xml/station/1}Elevation').text,
                'creationDate' : station.find('.//{http://www.fdsn.org/xml/station/1}CreationDate').text
            })
    return data

@station.route('/query', methods=['GET'])
def query():
    args = {}
    if 'net' in request.args :
        args['network'] = request.args['net']
    if 'start' in request.args :
        args['starttime'] = request.args['start']
    if 'end' in request.args : 
        args['endtime'] = request.args['end']
    if 'sta' in request.args : 
        args['station'] = request.args['sta']
    if 'cha' in request.args :
        args['channel'] = request.args['cha']
    if 'loc' in request.args :
        args['location'] = request.args['loc']
    if 'maxlat' in request.args :
        args['maxlatitude'] = request.args['maxlat']
    if 'minlat' in request.args :
        args['minlatitude'] = request.args['minlat']
    if 'maxlon' in request.args :
        args['maxlongitude'] = request.args['maxlon']
    if 'minlon' in request.args :
        args['minlongitude'] = request.args['minlon']
    if 'lat' in request.args :
        args['latitude'] = request.args['lat']
    if 'lon' in request.args :
        args['longitude'] = request.args['lon']
    if 'minradius' in request.args:
        args['minradius'] = request.args['minradius']
    if 'maxradius' in request.args:
        args['maxradius'] = request.args['maxradius']
    return getStation(args)



###### Functions #######
def getStation(args):
    file = ET.parse(XML_FOLDER_SERVER+'stations.xml')
    root = file.getroot()
    data = {'stations' : []}
    for network in root.findall(".//{http://www.fdsn.org/xml/station/1}Network"):
        if ('network' in args and network.get('code') == args['network']) or 'network' not in args:
            for station in network.findall(".//{http://www.fdsn.org/xml/station/1}Station"):
                if checkArgs(args, network, station):
                    data['stations'].append({
                        'station_code' : station.get('code').upper(),
                        'latitude' : station.find('.//{http://www.fdsn.org/xml/station/1}Latitude').text,
                        'longitude' : station.find('.//{http://www.fdsn.org/xml/station/1}Longitude').text,
                        'elevation' : station.find('.//{http://www.fdsn.org/xml/station/1}Elevation').text,
                        'creationDate' : station.find('.//{http://www.fdsn.org/xml/station/1}CreationDate').text,
                        'network_code' : network.get('code'),
                        'network_startDate' : network.get('startDate')
                    });
    return data

def checkArgs(args, network, station):
    res = True
    if len(args) == 0:
        return res
    elif 'station' in args and station.get('code').upper() != args['station'].upper():
        res = False
    elif res and 'starttime' in args and args['starttime'] > station.find('.//{http://www.fdsn.org/xml/station/1}CreationDate').text:
        res = False
    elif res and 'endtime' in args and args['endtime'] < station.find('.//{http://www.fdsn.org/xml/station/1}CreationDate').text:
        res = False
    elif 'maxradius' in args:
        if 'latitude' in args :
            lat = float(station.find('.//{http://www.fdsn.org/xml/station/1}Latitude').text)
            print()
            if lat > float(args['latitude'])+float(args['maxradius']) or lat < float(args['latitude'])-float(args['maxradius']):
                res = False
        if 'longitude' in args :
            lon = float(station.find('.//{http://www.fdsn.org/xml/station/1}Longitude').text)
            if lon > float(args['longitude'])+float(args['maxradius']) or lon < float(args['longitude'])-float(args['maxradius']):
                res = False
    return res


