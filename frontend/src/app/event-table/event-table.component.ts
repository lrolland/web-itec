import { OnInit, Component, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { EventTableDataSource, EventTableItem } from './event-table-datasource';
import { RequestService } from '../request.service';
import { FormComponent } from '../form/form.component';


@Component({
  selector: 'app-event-table',
  templateUrl: './event-table.component.html',
  styleUrls: ['./event-table.component.css']
})
export class EventTableComponent implements OnInit {
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatTable) table!: MatTable<EventTableItem>;
  dataSource: EventTableDataSource;
  formControl : FormComponent;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['place', 'date', 'magnitude'];

  constructor(private requestservice : RequestService) {
    this.dataSource = new EventTableDataSource(this.requestservice);
    this.formControl = new FormComponent(this.requestservice);
  }

  ngOnInit(): void {
    this.requestservice.getEvents().subscribe(data => {
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.table.dataSource = this.dataSource;
    })
  }

  /**
   * The on-click function, which start the request
   * @param row : {}
   */
  searchStation(row : {}) : void {
    this.formControl.stationQuery(row);
  }
}
