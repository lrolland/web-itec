import { Injectable } from '@angular/core';
import * as L from 'leaflet';
import { PopupService } from './popup.service'; 
import { RequestService } from './request.service';

@Injectable({
  providedIn: 'root'
})
export class MarkerService {

  constructor(private popupservice: PopupService, private requestservice: RequestService) {
  }

  makeMarkers(map : L.Map): void {
    this.requestservice.getEvents().subscribe((res: any) => {
      for (const c of res.features) {
        const lat = c.geometry.coordinates[0];
        const lon = c.geometry.coordinates[1];
        const marker = L.marker([lat, lon]);
        marker.bindPopup(this.popupservice.makeEventPopup(c.properties))
        marker.addTo(map);
      }
    });
  }
}
