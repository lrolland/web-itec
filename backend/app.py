from flask import Flask, Blueprint
from flask_cors import CORS
from webservices.dataselect import dataselect
from webservices.station import station
from webservices.event import event

app = Flask(__name__)
app.config["DEBUG"] = True

app.register_blueprint(dataselect, url_prefix='/fdsnws/dataselect')
app.register_blueprint(station, url_prefix='/fdsnws/station')
app.register_blueprint(event, url_prefix='/fdsnws/event')

CORS(app)

@app.route('/fdsnws', methods=['GET'])
def home():
    return '<h1>Welcome to ITEC</h1>'

@app.route('/fdsnws/application.wadl', methods=['GET'])
def wadl():
    return 'Sorry, no wadl yet'

if __name__ == '__main__':
    app.run()