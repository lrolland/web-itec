import xml.etree.ElementTree as ET
from flask import Blueprint, send_from_directory, jsonify

event = Blueprint('event', __name__)

XML_FOLDER_SERVER = '/var/www/web-itec/backend/xml/'
XML_FOLDER = './backend/xml/'

@event.route('/', methods=['GET'])
def home():
    return '<h1>Event</h1>'

@event.route('/version', methods=['GET'])
def version():
    return 'version : 0'

@event.route('/<eventName>', methods=['GET'])
def readFile(eventName):
    return send_from_directory('event_xml/', filename=eventName, as_attachment=False)

@event.route('/<filename>.geoJSON', methods=['GET'])
def fromXmlToGeoJSON(filename):
    return readQuakemlToGeoJSON(filename+'.xml')


def getValue(tag, value):
    temp = tag.find('{http://quakeml.org/xmlns/bed/1.2}'+value)
    if value == 'creationInfo' :
        val = temp.find('{http://quakeml.org/xmlns/bed/1.2}author').text 
    else:
        val = temp.find('{http://quakeml.org/xmlns/bed/1.2}value').text
    return val


def readQuakemlToGeoJSON(name):
    file = ET.parse(XML_FOLDER_SERVER+name)
    root = file.getroot()
    eventParameters = root.find('{http://quakeml.org/xmlns/bed/1.2}eventParameters')
    data = {
        "type": "FeatureCollection",
        "features": []
    }
    for i in eventParameters.findall(".//{http://quakeml.org/xmlns/bed/1.2}event"):
        origin = i.find('{http://quakeml.org/xmlns/bed/1.2}origin')
        magnitude = i.find('{http://quakeml.org/xmlns/bed/1.2}magnitude')
        description = i.find('{http://quakeml.org/xmlns/bed/1.2}description')
        data["features"].append({
            'type' : 'Feature',
            'geometry' : {
                'type' : 'Point',
                'coordinates' : [getValue(origin, 'latitude'), getValue(origin, 'longitude')]
            },
            'properties' : {
                'name' : description.find('{http://quakeml.org/xmlns/bed/1.2}type').text,
                'place' : description.find('{http://quakeml.org/xmlns/bed/1.2}text').text,
                'event_type' : i.find('{http://quakeml.org/xmlns/bed/1.2}type').text,
                'origin' : {
                    'time' : getValue(origin, 'time'),
                    'depth' : getValue(origin, 'depth'),
                    'creationInfo' : getValue(origin, 'creationInfo')
                },
                'magnitude' : {
                    'mag' : getValue(magnitude, 'mag'),
                    'type' : magnitude.find('{http://quakeml.org/xmlns/bed/1.2}type').text,
                    'creationInfo' : getValue(magnitude, 'creationInfo')
                }
            }
        })
    return data
