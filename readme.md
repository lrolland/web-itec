# Welcome to ITEC
This project has been created by Lucie Rolland (mail)
The actual prototype has been realised by Arthur Fontaine (arthur.fontaine@icloud.com)

This prototype is separeted in two, the backend in python allow to store, make request through the SDS architecture, or the different xml files (which are being used as a database) and sending the results.
And the second part, made with angular, leaflet, and table-material, allow to show different events requested from the api, on a map, to search for the stations around, which give us the location and then we can search for the data.

# Development Installation 
## Backend :
	* install anaconda
	* install obspy
	* install python3
	* install flask and flask-cors

## Frontend : 
	* install npm
	* install nodes : 12.18.2
	* install angular : 11.2.11

# Update data :
To update of modify data in the SDS or inside the xml files, just clone the git repository (if it's not), and do want you want, then push your changes to the git.

# Modify the code
## To modify api
To modify the api, just make your change in the project and then push them. Don't forget to pull the changes on the server.

## To modify the ui 
This time it is a little different, after being done with your changes, you need to have angular installed in order to build the application (which will go into the dist directory), and then push your changes and pull them in the server.

# Dealing with gitlab
You need to download git (methods can change depending on your os). For windows, use git bash from https://gitforwindows.org/. 

## Configure git and initialise the access with a secure ssh key
In terminal (or in git bash for windows) type `ssh-keygen -o -t rsa -b 4096 -C "machine_identifier"` with default parameters (no passphrase). This will create a private and public key. Copy paste the public key `cat .ssh/id_rsa.pub` and add it to your profile on gitlab.oca.eu (https://gitlab.oca.eu/-/profile/keys).

## Clone the repository
Use the ssh git clone: `git clone git@gitlab.oca.eu:lrolland/web-itec.git`

## Modify the code locally
Then you need to be in the folder containing .git files (you may need to show hidden files).
Now you can use the command 

    git add .

to tell you want to add/modify everything in this folder ( if you want, you can replace "." with a path to a file/folder )
Then you will have to use 

    git commit -m "<yourMessage>"

This will commit the changes and the message is used to explain what you did

## Update the remote repository
Finally, 

    git push

will push your code into the gitlab.

Connect to server :

	ssh root@itec.unice.fr

Then in order to pull in the server, just use 

	git pull

And then use to restart server files

	service httpd restart

# Authors
Arthur Fontaine (arthur_fontaine@icloud.com): developer

Lucie Rolland (lrolland@geoazur.unice.fr): creator of ITEC

Edhah Munaibari (munaibari@geoazur.unice.fr): data producer
